﻿using Lab7.AddressControl.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Lab7.AddressControl
{
    public class AddressBufor : IAddress
    {
        public event EventHandler<AddressChangedArgs> AddressChanged;

        Control control;
        public Control Control
        {
            get { return control; }
        }

        public AddressBufor()
        {
            control = new AddressUserControl(this);
        }

        public void OnAddressChanged(AddressChangedArgs e)
        {
            EventHandler<AddressChangedArgs> handler = AddressChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
