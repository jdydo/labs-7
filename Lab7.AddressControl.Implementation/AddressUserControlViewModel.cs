﻿using Lab7.AddressControl.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace Lab7.AddressControl
{
    internal class AdresUserControlViewModel : ViewModel
    {
        private string addressURL;
        public string AddressURL
        {
            get { return addressURL; }
            set
            {
                addressURL = value;
                FirePropertyChanged("AddressURL");
            }
        }

        private Command commandClick;
        public Command CommandClick
        {
            get { return commandClick; }
        }

        private void DoClick(object parameter)
        {
            AddressChangedArgs args = new AddressChangedArgs();
            args.URL = AddressURL;
            bufor.OnAddressChanged(args);
        }

        AddressBufor bufor;
        public AdresUserControlViewModel(AddressBufor bufor)
        {
            commandClick = new Command(DoClick);
            this.bufor = bufor;
        }
    }
}
