﻿using Lab7.AddressControl.Contract;
using Lab7.Infrastructure;
using Lab7.RemoteImageControl.Contract;
using Lab7.RemoteImageControl.Implementation;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Lab7.Main
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            var container = Configuration.ConfigureApp();

            var address = container.Resolve<IAddress>();
            Grid.SetRow(address.Control, 0);
            var rdef = new RowDefinition();
            rdef.Height = new GridLength(30);
            this.Panel.RowDefinitions.Add(rdef);
            this.Panel.Children.Add(address.Control);

            var image = container.Resolve<IRemoteImage>();
            ((RemoteImageBufor)image).Started += LoadStarted; ////
            ((RemoteImageBufor)image).Stoped += LoadStoped;  ////
            Grid.SetRow(image.Control, 1);
            this.Panel.RowDefinitions.Add(new RowDefinition());
            this.Panel.Children.Add(image.Control);

            ///////////////////////////////////////////////////
            var image1 = container.Resolve<IRemoteImage>();
            ((RemoteImageBufor)image1).Started += LoadStarted;
            ((RemoteImageBufor)image1).Stoped += LoadStoped;
            Grid.SetRow(image1.Control, 2);
            this.Panel.RowDefinitions.Add(new RowDefinition());
            this.Panel.Children.Add(image1.Control);

            var image2 = container.Resolve<IRemoteImage>();
            ((RemoteImageBufor)image2).Started += LoadStarted;
            ((RemoteImageBufor)image2).Stoped += LoadStoped;
            Grid.SetRow(image2.Control, 3);
            this.Panel.RowDefinitions.Add(new RowDefinition());
            this.Panel.Children.Add(image2.Control);
            ////////////////////////////////////////////////////

            this.Panel.ShowGridLines = true;
        }

        private void LoadStarted(object sender, EventArgs e)
        {
            MessageBox.Show("Wczytywanie rozpoczęte");
        }

        private void LoadStoped(object sender, EventArgs e)
        {
            MessageBox.Show("Wczytywanie zakończone");
        }
    }
}
