﻿using Lab7.AddressControl.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using TinyMVVMHelper;

namespace Lab7.RemoteImageControl.Implementation
{
    internal class RemoteImageUserControlViewModel : ViewModel
    {
        ImageSource imageSourcee;
        public ImageSource ImageSourcee
        {
            get { return imageSourcee; }
            set
            {
                imageSourcee = value;
                FirePropertyChanged("ImageSourcee");
            }
        }

        IAddress address;
        public RemoteImageUserControlViewModel(IAddress address)
        {
            this.address = address;
        }
    }
}
