﻿using Lab7.AddressControl.Contract;
using Lab7.RemoteImageControl.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace Lab7.RemoteImageControl.Implementation
{
    public class RemoteImageBufor : IRemoteImage
    {
        Control control;
        public Control Control
        {
            get { return control; }
        }

        public async void Load(string url)
        {
            OnStarted();
            //MemoryStream ms = new MemoryStream(new WebClient().DownloadData(new Uri(url)));
            //ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
            //ImageSource imageSource = (ImageSource)imageSourceConverter.ConvertFrom(ms);
            ((RemoteImageUserControlViewModel)((RemoteImageUserControl)control).DataContext).ImageSourcee = await LoadImageSourceAsync(url);
            OnStoped();
        }

        ///////////////////////////////////////////////////////////////////////////////////
        public async Task LoadAsync(string url)
        {
            OnStarted();
            //MemoryStream ms = new MemoryStream(new WebClient().DownloadData(new Uri(url)));
            //ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
            //ImageSource imageSource = (ImageSource)imageSourceConverter.ConvertFrom(ms);
            ((RemoteImageUserControlViewModel)((RemoteImageUserControl)control).DataContext).ImageSourcee = await LoadImageSourceAsync(url);
            OnStoped();
        }
        ///////////////////////////////////////////////////////////////////////////////////

        private async Task<ImageSource> LoadImageSourceAsync(string address)
        {
            ImageSource imgSource = null;

            try
            {
                MemoryStream ms = new MemoryStream(await new WebClient().DownloadDataTaskAsync(new Uri(address)));
                ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
                imgSource = (ImageSource)imageSourceConverter.ConvertFrom(ms);
            }
            catch (Exception)
            {
            }

            return imgSource;
        }

        IAddress address;
        public RemoteImageBufor(IAddress address)
        {
            this.address = address;
            control = new RemoteImageUserControl(address);
            //address.AddressChanged += address_AddressChanged;
            address.AddressChanged += address_AddressChangedAsync;
        }

        void address_AddressChanged(object sender, AddressChangedArgs e)
        {
            Load(e.URL);
        }

        ////////////////////////////////////////////////////////////////////////////
        async void address_AddressChangedAsync(object sender, AddressChangedArgs e)
        {
            await LoadAsync(e.URL);
        }
        ////////////////////////////////////////////////////////////////////////////


        public event EventHandler Started;
        public void OnStarted()
        {
            EventHandler handler = Started;
            if (handler != null)
            {
                handler(this, null);
            }
        }

        public event EventHandler Stoped;
        public void OnStoped()
        {
            EventHandler handler = Stoped;
            if (handler != null)
            {
                handler(this, null);
            }
        }
    }
}
